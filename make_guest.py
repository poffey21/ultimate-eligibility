from envparse import Env
from gitlab import Gitlab as GitLab
import requests
import json
import os

DEBUG_COUNT = 50


env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')


def main():
    private_token = env('PRIVATE_TOKEN')
    origin = env('GITLAB_HOST')
    gl = GitLab(url=origin, private_token=private_token)
    gl.auth()

    # Get list of all top_level_groups
    top_level_groups = []
    for x in gl.groups.list(top_level_only=True, all=True):
        top_level_groups.append(x.id)
    
    # Get list of all users that are eligible for guest
    with open('output_id.json', 'r') as f:
        data = json.loads(f.read())

    for group_id in top_level_groups:
        for user_id in data['eligible_for_guest']:
            url = f'{origin}api/v4/groups/{group_id}/billable_members/{user_id}/memberships'
            r = requests.get(url, headers={'PRIVATE-TOKEN': private_token})
            if not r.ok:
                if r.json()['message'] == '404 User Not Found':
                    continue  # User is not in this top-level group
                else:
                    print(f'Response is bad when looking for Billable Members at {url}:', r.content)
                    exit(1)
            memberships = r.json()
            for membership in memberships:
                if membership['access_level']['integer_value'] >= 20:
                    # This means they have explicit access to this group or project, and that the
                    # level of access is Reporter or above.
                    # https://docs.gitlab.com/ee/api/members.html#valid-access-levels
                    # 
                    # TODO: Change user's membership in this project (or group) to be Guest, explicitly.
                    if "/-/project_members" in membership['source_members_url']:
                        source_id = membership['source_id']
                        url = f'{origin}api/v4/projects/{source_id}/members/{user_id}/?access_level=10'
                        requests.put(url, headers={'PRIVATE-TOKEN': private_token})
                        print(f'MAKING USER ({user_id}) A GUEST IN A PROJECT', url)
                    elif "/-/group_members" in membership['source_members_url']:
                        source_id = membership['source_id']
                        url = f'{origin}api/v4/groups/{source_id}/members/{user_id}/?access_level=10'
                        requests.put(url, headers={'PRIVATE-TOKEN': private_token})
                        print(f'MAKING USER ({user_id}) A GUEST IN A GROUP', url)
                    else:
                        pass
                

if __name__ == '__main__':
    main()
